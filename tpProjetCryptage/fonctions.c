#include <stdio.h>
#include <stdlib.h>

#include "header.h"


void rCrypt()
{
    FILE* fSource;
    char filename[128]="";
    printf("\nRenseigner le nom du fichier. CE FICHIER DOIT SE TROUVER DANS LE MEME DOSSIER QUE CELUI DU SCRIPT\n");
    scanf("%s", &filename);


    fSource = fopen(filename, "rt");
    if(fSource==NULL)
    {
        printf("\n%s\ FICHIER INTROUVABLE", filename);
        exit(1);
    }
    int i = 0;

    fseek(fSource, 0, SEEK_END);
    int lgSrc = ftell(fSource);
    fseek(fSource, 0, SEEK_SET);

    char tSrc[lgSrc];

    while(!feof(fSource))
    {
        fread(&tSrc[i], sizeof(char), 1, fSource);
        //printf("\ncara1[%d] = %c", i, tSrc[i]);
        i++;

        if(feof(fSource))
        {
            printf("\nFin Lecture Fichier!!!");
        }
    }

    int resS = fclose(fSource);
    if(resS != 0)
    {
        printf("\nErreur!!!");
        exit(EXIT_FAILURE);
    }

    FILE* fPero;
    fPero = fopen("peroq.def", "rt");
    int j = 0;

    fseek(fPero, 0, SEEK_END);
    int lgPero = ftell(fPero);
    fseek(fPero, 0, SEEK_SET);

    char tPero[lgPero];

    while(!feof(fPero))
    {
        fread(&tPero[j], sizeof(char), 1, fPero);
        //printf("\ncara1[%d] = %c", j, tPero[j]);
        j++;

        if(feof(fPero))
        {
            printf("\nFin Lecture Fichier!!!");
        }
    }

    int resP = fclose(fPero);
    if(resP != 0)
    {
        printf("\nErreur!!!");
        exit(EXIT_FAILURE);
    }

    FILE* fCrypt;
    fCrypt = fopen("sourceCrypt.txt", "wt");
    char tCrypt[lgSrc];

    int l = 0;
    int m = 0;
    int n;
    for(n = 0 ; (l <= lgSrc-1); n++)
    {
        if(m <= lgPero-1)
        {
            tCrypt[n]=(tSrc[l]-tPero[m]);
            //printf("\n%d %c", l, tSrc[l]);
            //printf("\n%d %c", m, tPero[m]);
            //printf("\n%d %c", n, tCrypt[n]);
            l++;
            m++;
        }
        else
        {
            tCrypt[n]=(tSrc[l]-tPero[m]);
            //printf("\n%d %c", l, tSrc[l]);
            //printf("\n%d %c", m, tPero[m]);
            //printf("\n%d %c", n, tCrypt[n]);;
            l++;
            m=0;
        }
        fwrite(&tCrypt[n], sizeof(char), 1, fCrypt);
        if(feof(fSource))
        {
            printf("\nFin Chiffrement!!!");
        }
    }

    remove(filename);

    printf("\nFin Chiffrement!!!");

    int resC = fclose(fCrypt);
    if(resC != 0)
    {
        printf("\nErreur!!!");
        exit(EXIT_FAILURE);
    }


}

void wPeroq()
{
    FILE* fPeroInit;
    fPeroInit = fopen("peroq.def", "wt");

    char wPer[100] = "";

    printf("\nEntrer votre perroquet: ");
    scanf("%s", &wPer);

    fwrite(wPer, 1, strlen(wPer), fPeroInit);

    int resP = fclose(fPeroInit);
    if(resP != 0)
    {
        printf("\nErreur!!!");
        exit(EXIT_FAILURE);
    }

}

void dCrypt()
{

    FILE* fPero;
    fPero = fopen("peroq.def", "rt");
    int j = 0;

    fseek(fPero, 0, SEEK_END);
    int lgPero = ftell(fPero);
    fseek(fPero, 0, SEEK_SET);

    char tPero[lgPero];

    while(!feof(fPero))
    {
        fread(&tPero[j], sizeof(char), 1, fPero);
        //printf("\ncara1[%d] = %c", j, tPero[j]);
        j++;

        if(feof(fPero))
        {
            printf("\nFin Lecture Fichier!!!");
        }
    }

    int resP = fclose(fPero);
    if(resP != 0)
    {
        printf("\nErreur!!!");
        exit(EXIT_FAILURE);
    }

    FILE* fCrypt;
    char filenameFCrypt[128]="";
    printf("\nRenseigner le nom du fichier a decrypter. CE FICHIER DOIT SE TROUVER DANS LE MEME DOSSIER QUE CELUI DU SCRIPT\n");
    scanf("%s", &filenameFCrypt);


    fCrypt = fopen(filenameFCrypt, "rt");
    if(fCrypt==NULL)
    {
        printf("\n%s\ FICHIER INTROUVABLE 1111", filenameFCrypt);
        exit(1);
    }
    int i = 0;

    fseek(fCrypt, 0, SEEK_END);
    int lgSrc = ftell(fCrypt);
    fseek(fCrypt, 0, SEEK_SET);

    char tSrc[lgSrc];

    while(!feof(fCrypt))
    {
        fread(&tSrc[i], sizeof(char), 1, fCrypt);
        //printf("\ncara1[%d] = %c", i, tSrc[i]);
        i++;

        if(feof(fCrypt))
        {
            printf("\nFin Lecture Fichier!!!");
        }
    }

    int resS = fclose(fCrypt);
    if(resS != 0)
    {
        printf("\nErreur!!!");
        exit(EXIT_FAILURE);
    }

    FILE* fDCrypt;
    fDCrypt = fopen("dCryptFile.txt", "wt");

    if(fDCrypt==NULL)
    {
        printf("\nFICHIER INTROUVABLE222222");
        exit(1);
    }

    char dCrypt[lgSrc];

    int l = 0;
    int m = 0;
    int n;
    for(n = 0 ; (l <= lgSrc-2); n++)
    {
        if(m <= lgPero-1)
        {
            dCrypt[n]=(tSrc[l]+tPero[m]);
            //printf("\n%d %c", l, tSrc[l]);
            //printf("\n%d %c", m, tPero[m]);
            //printf("\n%d %c", n, dCrypt[n]);
            l++;
            m++;
        }
        else
        {
            dCrypt[n]=(tSrc[l]+tPero[m]);
            //printf("\n%d %c", l, tSrc[l]);
            //printf("\n%d %c", m, tPero[m]);
            //printf("\n%d %c", n, dCrypt[n]);;
            l++;
            m=0;
        }
        fwrite(&dCrypt[n], sizeof(char), 1, fDCrypt);
        if(feof(fDCrypt))
        {
            printf("\nFin Chiffrement!!!");
        }
    }
}
