#include <stdio.h>
#include <stdlib.h>

#include "header.h"
int main()
{
    char answer0;
    printf("\nAvez vous un fichier a dechiffrer?(O/N)");
    scanf("\n %s", &answer0);
    switch(answer0)
    {
        case 'O':
        case 'o':
            dCrypt();
            break;
        case 'N':
        case 'n':
            printf("\nAvez vous une clef perroquet?(O/N)");
            char answer1;
            scanf("\n %s", &answer1);

            switch(answer1)
            {
                case 'O':
                case 'o':
                    printf("\nClef perroquet existante");
                    break;
                case 'N':
                case 'n':
                    wPeroq();
                    break;
                default:
                    printf("\nErreur de saisie!!!");
                    break;
            }

            char answer2;
            printf("\nVoulez-vous chiffrer un fichier?(O/N)");
            scanf("\n %s", &answer2);
            switch(answer2)
            {
                case 'O':
                case 'o':
                    rCrypt();
                    break;
                case 'N':
                case 'n':
                    printf("\nComme vous voulez.");
                    break;
                default:
                    printf("\nErreur de saisie!!!");
                    break;
            }
            break;

            default:
                printf("\nErreur de saisie!!!");
            break;
    }
    return EXIT_SUCCESS;
}

